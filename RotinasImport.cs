using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Cobranca.Rotinas
{
    public class RotinaImportacao
    {
        Dados _qry = new Dados();
        string CodImportacao = "";
        public void MoverFile(string NomeArquivo, string CaminhoOriginal, string CaminhoDestino, string ID)
        {
            string PastaFinal = "";

            if (!string.IsNullOrEmpty(ID))
                PastaFinal = Path.Combine(CaminhoDestino, ID);
            else
                PastaFinal = Path.Combine(CaminhoDestino);

            if (!Directory.Exists(PastaFinal))
                Directory.CreateDirectory(PastaFinal);

            string DestinoFinal = Path.Combine(PastaFinal, NomeArquivo);

            if (!File.Exists(DestinoFinal))
                File.Move(CaminhoOriginal, DestinoFinal);
        }
        public OperacaoDbRetorno ProcessaCliente(int CodImportacao = 0)
        {
            return _qry.ProcessarCliente(CodImportacao);
        }
        public OperacaoDbRetorno ProcessaClienteEndereco(int CodImportacao = 0)
        {
            return _qry.ProcessarClienteEndereco(CodImportacao);
        }
        public OperacaoDbRetorno ProcessaParcela(int CodImportacao = 0)
        {
            OperacaoDbRetorno opEtapa1 = _qry.ProcessarParcelaEtapaContrato(CodImportacao);
            OperacaoDbRetorno opEtapaParcela = new OperacaoDbRetorno() { OK = false };
            if (opEtapa1.OK)
            {
                opEtapaParcela = _qry.ProcessarParcelaEtapaProcessarParcela(CodImportacao);

            }
            return opEtapaParcela;
        }
        public OperacaoDbRetorno ProcessarHistoricoParcelaPorCodImportacao(int CodImportacao = 0)
        {
            OperacaoDbRetorno retorno = _qry.ProcessarParcelaEtapaHistorico(CodImportacao);

            return retorno;
        }
        public OperacaoDbRetorno ProcessarInformacaoParcelaPorCodImportacao(int CodImportacao = 0)
        {
            OperacaoDbRetorno retorno = _qry.ProcessarParcelaEtapaComputarInformacoes(CodImportacao);

            return retorno;
        }
        public OperacaoDbRetorno ProcessaCredito(int CodImportacao = 0)
        {
            return _qry.ProcessarCredito(CodImportacao);
        }
        public OperacaoDbRetorno ProcessaBoleto(int CodImportacao = 0)
        {
            return _qry.ProcessarBoleto(CodImportacao);
        }
        public string CarregarArquivoParaBanco(string NomeArquivo, string CaminhoArquivo, short Tipo)
        {
            var l = new List<int>();
            return CarregarArquivoParaBanco(NomeArquivo, CaminhoArquivo, Tipo, ref l);
        }
        public string CarregarArquivoParaBanco(string NomeArquivo, string CaminhoArquivo, short Tipo, ref List<int> linhasComErro)
        {
            try
            {
                linhasComErro = new List<int>();
                var dt = ConvertTXTtoDataTable(NomeArquivo, CaminhoArquivo, Tipo, ref linhasComErro);
                Repository<Usuario> _repUsuario = new Repository<Usuario>();
                var usr = _repUsuario.All().Where(x => x.Login == "Servico").FirstOrDefault();
                if (usr == null)
                {
                    usr = Activator.CreateInstance<Usuario>();
                    usr.Nome = "Importacao Servico";
                    usr.Email = "Importacao Servico";
                    usr.Login = "Servico";
                    usr.Senha = "1";
                    usr.PerfilTipo = 1;
                    usr.SistemaCobranca = true;
                    usr.DataCadastro = DateTime.Now;
                    usr = _repUsuario.CreateOrUpdate(usr);
                }
                var colunasDePara = new List<ColunaDePara>();

                if (Tipo == 1)
                {
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerocliente", NomeColunaTabela = "NumeroCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "decriçãocliente", NomeColunaTabela = "DescricaoCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cpfcnpj", NomeColunaTabela = "CpfCnpj" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "endereço", NomeColunaTabela = "Endereco" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numero", NomeColunaTabela = "Numero" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complemento", NomeColunaTabela = "Complemento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairro", NomeColunaTabela = "Bairro" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cep", NomeColunaTabela = "CEP" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidade", NomeColunaTabela = "Cidade" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estado", NomeColunaTabela = "Estado" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "tp", NomeColunaTabela = "TP" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "ddd", NomeColunaTabela = "DDD" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerofone", NomeColunaTabela = "NumeroTelefone" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "eaemal", NomeColunaTabela = "Email" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Duplicado", NomeColunaTabela = "Duplicado" });

                    ImportacaoDb.BulkCopy(dt, "ImportacaoCliente", colunasDePara);

                    var listaResultado = ImportacaoDb.ImportacaoClienteObterStatus(usr.Cod, 0);//38 - USUARIO IMPORTACAO SERVICO
                    CodImportacao = DbRotinas.ConverterParaString(listaResultado.Dados);
                }
                else if (Tipo == 2)
                {
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "companhia", NomeColunaTabela = "Companhia" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Filial", NomeColunaTabela = "Filial" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Contrato", NomeColunaTabela = "NumeroContrato" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Descricaodafilial", NomeColunaTabela = "FilialDescricao" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "rotaarea", NomeColunaTabela = "RotaArea" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Status", NomeColunaTabela = "StatusContrato" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Tipodecontrato", NomeColunaTabela = "TipoContrato" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Numerodocliente", NomeColunaTabela = "NumeroCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "CPFCNPJ", NomeColunaTabela = "CPFCNPJ" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Descricao", NomeColunaTabela = "Descricao" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "NumerodaFatura", NomeColunaTabela = "NumeroFatura" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valorfatura", NomeColunaTabela = "ValorFatura" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valoremaberto", NomeColunaTabela = "ValorAberto" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datavencimento", NomeColunaTabela = "DataVencimento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datafechamento", NomeColunaTabela = "DataFechamento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerodaparcela", NomeColunaTabela = "NumeroParcela" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "governo", NomeColunaTabela = "Governo" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "corporativo", NomeColunaTabela = "Corporativo" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "classificacaofatura", NomeColunaTabela = "CodSinteseCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "notafiscal", NomeColunaTabela = "NotaFiscal" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "tipodocumento", NomeColunaTabela = "TipoDocumento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "dataemissaofatura", NomeColunaTabela = "DataEmissao" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "instrumentopagto", NomeColunaTabela = "InstrumentoPgto" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datapagamento", NomeColunaTabela = "DataPagamento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "nossonumero", NomeColunaTabela = "NossoNumero" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "empresacobranca", NomeColunaTabela = "EmpresaCobranca" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "an8parcela", NomeColunaTabela = "NumeroClienteParcela" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerodalinha", NomeColunaTabela = "NumeroLinha" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "impostos", NomeColunaTabela = "Imposto" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "companhiafiscal", NomeColunaTabela = "CompanhiaFiscal" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "clienteVIP", NomeColunaTabela = "ClienteVIP" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "CodContrato", NomeColunaTabela = "CodContrato" });

                    ImportacaoDb.BulkCopy(dt, "ImportacaoParcela", colunasDePara);
                    var listaResultado = ImportacaoDb.ImportacaoParcelaObterStatus(usr.Cod, 0, NomeArquivo);//38 - USUARIO IMPORTACAO SERVICO
                    CodImportacao = DbRotinas.ConverterParaString(listaResultado.Dados);
                }
                else if (Tipo == 3)
                {
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numero do documento", NomeColunaTabela = "NumeroDocumento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "nr cliente", NomeColunaTabela = "NumeroCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "descrição", NomeColunaTabela = "Descricao" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "data do credito", NomeColunaTabela = "DataCredito" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valor do credito", NomeColunaTabela = "ValorCredito" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "obs", NomeColunaTabela = "Observacao" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "saldo do credito", NomeColunaTabela = "SaldoCredito" });
                    ImportacaoDb.BulkCopy(dt, "ImportacaoCredito", colunasDePara);
                    var listaResultado = ImportacaoDb.ImportacaoCreditoObterStatus(usr.Cod, 0);//38 - USUARIO IMPORTACAO SERVICO
                    CodImportacao = listaResultado.FirstOrDefault().ID;
                }
                else if (Tipo == 4) //BOLETO
                {
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Companhia", NomeColunaTabela = "Filial" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "NumerodaFatura", NomeColunaTabela = "NumeroFatura" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Numerodocliente", NomeColunaTabela = "NumeroCliente" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datavencimento", NomeColunaTabela = "DataVencimento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valorfatura", NomeColunaTabela = "ValorFatura" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "linhadigitavel", NomeColunaTabela = "LinhaDigitavel" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datadocumento", NomeColunaTabela = "DataDocumento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "endereço", NomeColunaTabela = "Endereco" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numero", NomeColunaTabela = "Numero" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complemento", NomeColunaTabela = "Complemento" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairro", NomeColunaTabela = "Bairro" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cep", NomeColunaTabela = "CEP" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidade", NomeColunaTabela = "Cidade" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estado", NomeColunaTabela = "Estado" });
                    ImportacaoDb.BulkCopy(dt, "ImportacaoBoleto", colunasDePara);
                    var listaResultado = ImportacaoDb.ImportacaoBoletoObterStatus(usr.Cod, 0, NomeArquivo);//38 - USUARIO IMPORTACAO SERVICO
                    CodImportacao = DbRotinas.ConverterParaString(listaResultado.Dados);
                }
                else if (Tipo == 5) //ENDEREÇO
                {
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Contrato", NomeColunaTabela = "Contrato" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "enderecocob", NomeColunaTabela = "EnderecoCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerocob", NomeColunaTabela = "NumeroCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complementocob", NomeColunaTabela = "ComplementoCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairrocob", NomeColunaTabela = "BairroCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cepcob", NomeColunaTabela = "CEPCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidadecob", NomeColunaTabela = "CidadeCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estadocob", NomeColunaTabela = "EstadoCob" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "enderecoobra", NomeColunaTabela = "EnderecoObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numeroobra", NomeColunaTabela = "NumeroObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complementoobra", NomeColunaTabela = "ComplementoObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairroobra", NomeColunaTabela = "BairroObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cepobra", NomeColunaTabela = "CepObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidadeobra", NomeColunaTabela = "CidadeObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estadoobra", NomeColunaTabela = "EstadoObra" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "mensagemcontrato", NomeColunaTabela = "MensagemContrato" });
                    colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "CodContrato", NomeColunaTabela = "CodContrato" });

                    ImportacaoDb.BulkCopy(dt, "ImportacaoClienteEndereco", colunasDePara);
                    var listaResultado = ImportacaoDb.ImportacaoClienteEnderecoObterStatus(usr.Cod, 0);//38 - USUARIO IMPORTACAO SERVICO
                    CodImportacao = DbRotinas.ConverterParaString(listaResultado.Dados);
                }
                return CodImportacao;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }



        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath, short tipo)
        {
            var l = new List<int>();
            return ConvertTXTtoDataTable(strFileName, strFilePath, tipo, ref l);
        }

        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath, short tipo, ref List<int> listaLinhaErro)
        {
            DataTable dt = new DataTable();
            try
            {

                using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
                {
                    if (tipo == 1) //IMPORTACAO DE CLIENTE
                    {
                        dt = TxtClienteEmDataTable(sr, dt);
                    }
                    else if (tipo == 2) //IMPORTACAO DE PARCELA
                    {
                        dt = TxtParcelaEmDataTable(sr, dt, ref listaLinhaErro);

                    }
                    else if (tipo == 3) //IMPORTACAO DE CREDITO
                    {
                        dt = TxtCreditoEmDataTable(sr, dt);
                    }
                    else if (tipo == 4) //IMPORTACAO DE BOLETO
                    {
                        dt = TxtBoletoEmDataTable(sr, dt);
                    }
                    else if (tipo == 5) //IMPORTACAO DE ENDERECO
                    {
                        dt = TxtEnderecoEmDataTable(sr, dt);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private static DataTable TxtClienteEmDataTable(StreamReader sr, DataTable dt)
        {
            try
            {
                Int32 c = 0;
                string[] headers = new string[] { };
                string linhaHader = sr.ReadLine();
                if (linhaHader.Split('\t').Length <= 1)
                {
                    headers = linhaHader.Split(new Char[] { ';' });
                }
                else
                {
                    headers = linhaHader.Split(new Char[] { '\t' });
                }
                foreach (string header in headers)
                {
                    if (dt.Columns.Count <= 15)
                    {
                        dt.Columns.Add(header.Trim());
                    }
                }
                //string[] rows = sr.ReadLine().Split('\t');

                while (true)
                {
                    DataRow dr = dt.NewRow();
                    string linhatexto = sr.ReadLine();
                    if (linhatexto == null)
                    {
                        break;
                    }
                    string[] quebra = new string[] { };
                    quebra = linhatexto.Split(new Char[] { ';' });
                    if (quebra.Count() <= 1)
                    {
                        quebra = linhatexto.Split(new Char[] { '\t' });
                    }
                    for (int i = 0; i < quebra.Count(); i++)
                    {
                        quebra[i] = quebra[i].Replace(";", "");
                        //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                        if (i <= 15)
                        {
                            if (i == 0 || i == 1 || i == 2 || i == 11 || i == 12 || i == 13)
                            {
                                if (i == 12)
                                {
                                    dr[i] = DbRotinas.Criptografar(quebra[i].Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Trim());
                                }
                                else
                                {
                                    dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                                }
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }
                        }


                    }
                    c = c + 1;

                    dt.Rows.Add(dr);
                }
                List<string> colunas = new List<string>();
                colunas.Add("numerocliente");
                colunas.Add("numerofone");
                DataTable tab = RemoveLinhasDuplicadasRows(dt, colunas);
                return dt;
            }
            catch (Exception ex)
            {

                var Msg = TranscreverErroAoLerArquivo(dt, Enumeradores.TipoImportacao.Cliente);

                throw new Exception($"{Msg}", ex);
            }

        }

        private static DataTable TxtParcelaEmDataTable(StreamReader sr, DataTable dt, ref List<int> listaLinhaErro)
        {
            listaLinhaErro = new List<int>();
            Repository<Contrato> _dbContrato = new Repository<Contrato>();
            try
            {
                string[] headers = new string[] { };
                string linhaHader = sr.ReadLine();
                if (linhaHader.Split('\t').Length <= 1)
                {
                    headers = linhaHader.Split(new Char[] { ';' });
                }
                else
                {
                    headers = linhaHader.Split(new Char[] { '\t' });
                }
                Int32 c = 0;
                foreach (string header in headers)
                {
                    if (dt.Columns.Count <= 30)
                    {
                        dt.Columns.Add(header.Trim());
                    }
                }

                //string[] rows = sr.ReadLine().Split(';');
                var text = sr.ReadToEnd();
                string[] stringSeparators = new string[] { "\r\n" };
                var rows = text.Split(stringSeparators, StringSplitOptions.None);

                int nLinha = 0;
                StringBuilder sbListaLinhaErros = new StringBuilder();
                // while (true)
                foreach (var r in rows)
                {
                    try
                    {
                        nLinha++;

                        DataRow dr = dt.NewRow();
                        //string linhatexto = sr.ReadLine();
                        string linhatexto = r.ToString();
                        if (linhatexto == null || String.IsNullOrEmpty(linhatexto))
                        {
                            break;
                        }

                        string[] quebra = new string[] { };
                        if (linhatexto.Split('\t').Length <= 1)
                        {
                            quebra = linhatexto.Split(new Char[] { ';' });
                        }
                        else
                        {
                            quebra = linhatexto.Split(new Char[] { '\t' });
                        }
                        //remove a ultima coluna
                        if (quebra.Count() == 32)
                        {
                            quebra = quebra.Take(quebra.Count() - 1).ToArray();
                        }

                        for (int i = 0; i < quebra.Count(); i++)
                        {
                            if (quebra.Count() != 31)
                                throw new Exception("erro na quantidade de informações.");

                            quebra[i] = quebra[i].Replace(";", "").Replace("\t", "").Replace("\"", "");
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 26)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else if (i == 2)//DESCRICAO FILIAL
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Replace(" SERAL", "").Trim());
                            }
                            //else if (i == 13 || i == 14 || i == 21 || i == 23)//PODE SER NULO
                            //{

                            //    dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                            //}
                            else if (i == 13 || i == 14 || i == 21 || i == 23)//DATA OBRIGATORIA
                            {
                                var val = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                                if (val == DateTime.MinValue)
                                {
                                    throw new Exception("o Valor da data é invalido");
                                }
                                dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                            }
                            else if (i == 11)
                            {
                                dr[i] = DbRotinas.ConverterParaDecimal(quebra[i].Trim());
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }
                        }
                        c = c + 1;


                        dt.Rows.Add(dr);
                    }
                    catch (Exception ex)
                    {
                        listaLinhaErro.Add(nLinha);
                    }
                }

                //ROTINA PARA IDENTIFICAR OS CONTRATOS E JA COLOCAR SEU ID
                List<Contrato> Contratos = _dbContrato.All().ToList();
                DataTable dtContrato = ToDataTable(Contratos.Select(x => new { CodContrato = x.Cod, NumeroContrato = x.NumeroContrato }).ToList());
                var result = ToDataTable((from datarow in dt.AsEnumerable()
                                          join datarow2 in dtContrato.AsEnumerable()
                                          on datarow.Field<string>("Contrato") equals datarow2.Field<string>("NumeroContrato") into dtJ
                                          from p in dtJ.DefaultIfEmpty()
                                          select new
                                          {
                                              companhia = DbRotinas.ConverterParaString(datarow["companhia"]),
                                              Filial = DbRotinas.ConverterParaString(datarow["Filial"]),
                                              Contrato = DbRotinas.ConverterParaString(datarow["Contrato"]),
                                              Descricaodafilial = DbRotinas.ConverterParaString(datarow["Descricaodafilial"]),
                                              rotaarea = DbRotinas.ConverterParaString(datarow["rotaarea"]),
                                              Status = DbRotinas.ConverterParaString(datarow["Status"]),
                                              Tipodecontrato = DbRotinas.ConverterParaString(datarow["Tipodecontrato"]),
                                              Numerodocliente = DbRotinas.ConverterParaString(datarow["Numerodocliente"]),
                                              CPFCNPJ = DbRotinas.ConverterParaString(datarow["CPFCNPJ"]),
                                              Descricao = DbRotinas.ConverterParaString(datarow["Descricao"]),
                                              NumerodaFatura = DbRotinas.ConverterParaString(datarow["NumerodaFatura"]),
                                              valorfatura = DbRotinas.ConverterParaString(datarow["valorfatura"]),
                                              valoremaberto = DbRotinas.ConverterParaString(datarow["valoremaberto"]),
                                              datavencimento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datavencimento"]),
                                              datafechamento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datafechamento"]),
                                              numerodaparcela = DbRotinas.ConverterParaString(datarow["numerodaparcela"]),
                                              governo = DbRotinas.ConverterParaString(datarow["governo"]),
                                              corporativo = DbRotinas.ConverterParaString(datarow["corporativo"]),
                                              classificacaofatura = DbRotinas.ConverterParaString(datarow["classificacaofatura"]),
                                              notafiscal = DbRotinas.ConverterParaString(datarow["notafiscal"]),
                                              tipodocumento = DbRotinas.ConverterParaString(datarow["tipodocumento"]),
                                              dataemissaofatura = DbRotinas.ConverterParaDatetimeOuNull(datarow["dataemissaofatura"]),
                                              instrumentopagto = DbRotinas.ConverterParaString(datarow["instrumentopagto"]),
                                              datapagamento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datapagamento"]),
                                              nossonumero = DbRotinas.ConverterParaString(datarow["nossonumero"]),
                                              empresacobranca = DbRotinas.ConverterParaString(datarow["empresacobranca"]),
                                              an8parcela = DbRotinas.ConverterParaString(datarow["an8parcela"]),
                                              numerodalinha = DbRotinas.ConverterParaString(datarow["numerodalinha"]),
                                              impostos = DbRotinas.ConverterParaString(datarow["impostos"]),
                                              companhiafiscal = DbRotinas.ConverterParaString(datarow["companhiafiscal"]),
                                              clienteVIP = DbRotinas.ConverterParaString(datarow["ClienteVIP"]),
                                              CodContrato = p == null ? 0 : DbRotinas.ConverterParaInt(p["CodContrato"]),
                                              ProcessamentoStatus = String.Empty,
                                              ProcessamentoMensagem = String.Empty
                                          }).ToList());

                return result;
            }
            catch (Exception ex)
            {
                var Msg = TranscreverErroAoLerArquivo(dt, Enumeradores.TipoImportacao.Parcela);

                throw new Exception($"{Msg}", ex);
            }

        }

        private static DataTable TxtCreditoEmDataTable(StreamReader sr, DataTable dt)
        {
            try
            {
                string[] headers = new string[] { };
                string linhaHader = sr.ReadLine();
                if (linhaHader.Split('\t').Length <= 1)
                {
                    headers = linhaHader.Split(new Char[] { ';' });
                }
                else
                {
                    headers = linhaHader.Split(new Char[] { '\t' });
                }
                Int32 c = 0;
                foreach (string header in headers)
                {
                    if (dt.Columns.Count <= 18)
                    {
                        dt.Columns.Add(header.Trim());
                    }
                }

                var text = sr.ReadToEnd();
                string[] stringSeparators = new string[] { "\r\n" };
                var rows = text.Split(stringSeparators, StringSplitOptions.None);
                //while (true)
                foreach (var r in rows)
                {
                    DataRow dr = dt.NewRow();

                    string linhatexto = r.ToString();
                    if (linhatexto == null || String.IsNullOrEmpty(linhatexto))
                    {
                        break;
                    }

                    string[] quebra = new string[] { };
                    if (linhatexto.Split(';').Length > 1)
                    {
                        quebra = linhatexto.Split(new Char[] { ';' });
                    }
                    else
                    {
                        quebra = linhatexto.Split(new Char[] { '\t' });
                    }
                    for (int i = 0; i < quebra.Count(); i++)
                    {
                        quebra[i] = quebra[i].Replace(";", "").Replace("\t", "").Replace("\"", "");
                        //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                        if (i == 1)
                        {
                            dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                        }
                        else
                        {
                            if (i == 3)
                            {
                                dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }

                        }
                    }
                    c = c + 1;

                    dt.Rows.Add(dr);
                }

                return dt;

            }
            catch (Exception ex)
            {
                var Msg = TranscreverErroAoLerArquivo(dt, Enumeradores.TipoImportacao.Credito);

                throw new Exception($"{Msg}", ex);
            }
        }

        private static DataTable TxtBoletoEmDataTable(StreamReader sr, DataTable dt)
        {
            try
            {
                string[] headers = new string[] { };
                string linhaHader = sr.ReadLine();
                headers = linhaHader.Split(new Char[] { ';' });
                //if (linhaHader.Split('\t').Length <= 1)
                //{
                //    headers = linhaHader.Split(new Char[] { ';' });
                //}
                //else
                //{
                //    headers = linhaHader.Split(new Char[] { '\t' });
                //}
                Int32 c = 0;
                foreach (string header in headers)
                {
                    if (dt.Columns.Count <= 13)
                    {
                        dt.Columns.Add(header.Trim());
                    }
                }

                while (true)
                {
                    DataRow dr = dt.NewRow();
                    string linhatexto = sr.ReadLine();
                    if (linhatexto == null)
                    {
                        break;
                    }
                    string[] quebra = new string[] { };
                    quebra = linhatexto.Split(new Char[] { ';' });
                    //if (linhatexto.Split('\t').Length <= 1)
                    //{
                    //    quebra = linhatexto.Split(new Char[] { ';' });
                    //}
                    //else
                    //{
                    //    quebra = linhatexto.Split(new Char[] { '\t' });
                    //}
                    //remove a ultima coluna
                    if (quebra.Count() == 15)
                    {
                        quebra = quebra.Take(quebra.Count() - 1).ToArray();
                    }
                    for (int i = 0; i < quebra.Count(); i++)
                    {
                        quebra[i] = quebra[i].Replace(";", "").Replace("\"", "");
                        //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                        if (i <= 13)
                        {
                            if (i == 2)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else
                            {
                                if (i == 3 || i == 6)
                                {
                                    dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                                }
                                else
                                {
                                    dr[i] = quebra[i].Trim();
                                }
                            }
                        }
                    }
                    c = c + 1;
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (Exception ex)
            {
                var Msg = TranscreverErroAoLerArquivo(dt, Enumeradores.TipoImportacao.Parcela);

                throw new Exception($"{Msg}", ex);
            }
        }

        private static DataTable TxtEnderecoEmDataTable(StreamReader sr, DataTable dt)
        {
            try
            {
                Repository<Contrato> _dbContrato = new Repository<Contrato>();
                Int32 c = 0;
                string[] headers = new string[] { };
                string linhaHader = sr.ReadLine();
                if (linhaHader.Split('\t').Length <= 1)
                {
                    headers = linhaHader.Split(new Char[] { ';' });
                }
                else
                {
                    headers = linhaHader.Split(new Char[] { '\t' });
                }

                foreach (string header in headers)
                {
                    if (dt.Columns.Count <= 17)
                    {
                        dt.Columns.Add(header.Trim());
                    }
                }

                while (true)
                {
                    DataRow dr = dt.NewRow();
                    string linhatexto = sr.ReadLine();
                    if (linhatexto == null)
                    {
                        break;
                    }
                    string[] quebra = new string[] { };
                    if (linhatexto.Split(';').Length > 1)
                    {
                        quebra = linhatexto.Split(new Char[] { ';' });
                    }
                    else
                    {
                        quebra = linhatexto.Split(new Char[] { '\t' });
                    }
                    //remove a ultima coluna
                    if (quebra.Count() == 19)
                    {
                        quebra = quebra.Take(quebra.Count() - 1).ToArray();
                    }
                    for (int i = 0; i < quebra.Count(); i++)
                    {
                        quebra[i] = quebra[i].Replace(";", "");
                        if (i <= 17)
                        {
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i == 0)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }
                        }
                    }
                    c = c + 1;

                    dt.Rows.Add(dr);
                }

                List<Contrato> Contratos = _dbContrato.All().ToList();
                DataTable dtContrato = ToDataTable(Contratos.Select(x => new { CodContrato = x.Cod, NumeroContrato = x.NumeroContrato }).ToList());
                var result = ToDataTable((from datarow in dt.AsEnumerable()
                                          join datarow2 in dtContrato.AsEnumerable()
                                          on datarow.Field<string>("Contrato") equals datarow2.Field<string>("NumeroContrato") into dtJ
                                          from p in dtJ.DefaultIfEmpty()
                                          select new
                                          {
                                              Contrato = DbRotinas.ConverterParaString(datarow["Contrato"]),
                                              enderecocob = DbRotinas.ConverterParaString(datarow["enderecocob"]),
                                              numerocob = DbRotinas.ConverterParaString(datarow["numerocob"]),
                                              complementocob = DbRotinas.ConverterParaString(datarow["complementocob"]),
                                              bairrocob = DbRotinas.ConverterParaString(datarow["bairrocob"]),
                                              cepcob = DbRotinas.ConverterParaString(datarow["cepcob"]),
                                              cidadecob = DbRotinas.ConverterParaString(datarow["cidadecob"]),
                                              estadocob = DbRotinas.ConverterParaString(datarow["estadocob"]),
                                              enderecoobra = DbRotinas.ConverterParaString(datarow["enderecoobra"]),
                                              numeroobra = DbRotinas.ConverterParaString(datarow["numeroobra"]),
                                              complementoobra = DbRotinas.ConverterParaString(datarow["complementoobra"]),
                                              bairroobra = DbRotinas.ConverterParaString(datarow["bairroobra"]),
                                              cepobra = DbRotinas.ConverterParaString(datarow["cepobra"]),
                                              cidadeobra = DbRotinas.ConverterParaString(datarow["cidadeobra"]),
                                              estadoobra = DbRotinas.ConverterParaString(datarow["estadoobra"]),
                                              mensagemcontrato = DbRotinas.ConverterParaString(datarow["mensagemcontrato"]),
                                              CodContrato = p == null ? 0 : DbRotinas.ConverterParaInt(p["CodContrato"])
                                          }).ToList());
                return result;
            }
            catch (Exception ex)
            {
                var Msg = TranscreverErroAoLerArquivo(dt, Enumeradores.TipoImportacao.Parcela);

                throw new Exception($"{Msg}", ex);
            }
        }
        public static DataTable RemoveLinhasDuplicadasRows(DataTable dTable, List<string> colunas)
        {
            Hashtable hTable = new Hashtable();
            ArrayList listDuplicada = new ArrayList();
            DataColumn Col = dTable.Columns.Add("Duplicado", Type.GetType("System.Boolean"));
            Col.SetOrdinal(14);// to put the column in position 0;
            try
            {
                foreach (DataRow drow in dTable.Rows)
                {
                    var texto = string.Empty;
                    //foreach (var c in colunas)
                    //{
                    //    texto = texto + (string)drow[c];
                    //}
                    //
                    //hTable.Add(texto, string.Empty);
                    drow[14] = false;
                    //if (hTable.Contains(texto))
                    //{
                    //    //listDuplicada.Add(drow);  ADICIONA NA LISTA DE DUPLICADOS
                    //    drow[14] = true;
                    //}
                    //else
                    //{
                    //    hTable.Add(texto, string.Empty);
                    //    drow[14] = false;
                    //}
                }


                ////Remove da lista datatable os itens duplicados.
                //foreach (DataRow dRow in listDuplicada)
                //    dTable.Rows.Remove(dRow);

                //Datatable which contains unique records will be return as output.
            }
            catch (Exception ex)
            {

                throw;
            }

            return dTable;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private static string TranscreverErroAoLerArquivo(DataTable dt, Enumeradores.TipoImportacao tipoImportacao)
        {
            try
            {
                int NrLinha = DbRotinas.ConverterParaInt(dt.Rows.Count) + 1; //1 - CABEÇALHO
                var strRowInfo = "";
                if (dt.Rows.Count > 0)
                {
                    DataRow lastRow = dt.Rows[dt.Rows.Count - 1];
                    if (tipoImportacao == Enumeradores.TipoImportacao.Cliente)
                    {
                        strRowInfo = $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["numerocliente"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["decriçãocliente"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["cpfcnpj"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["endereço"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numero"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["complemento"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["bairro"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cep"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cidade"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["estado"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["tp"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["ddd"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numerofone"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["eaemal"])};";
                    }
                    else if (tipoImportacao == Enumeradores.TipoImportacao.Parcela)
                    {
                        strRowInfo = $"{DbRotinas.ConverterParaString(lastRow["Companhia"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["Filial"])};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Descricaodafilial"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Contrato"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["rotaarea"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["Status"])};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Tipodecontrato"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Numerodocliente"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["CPFCNPJ"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Descricao"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["NumerodaFatura"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["valorfatura"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["valoremaberto"])};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["datavencimento"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["datafechamento"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numerodaparcela"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["governo"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["corporativo"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["classificacaofatura"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["notafiscal"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["tipodocumento"])};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["dataemissaofatura"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaString(lastRow["instrumentopagto"])};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["datapagamento"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaString(lastRow["nossonumero"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["empresacobranca"])};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["an8parcela"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numerodalinha"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["impostos"])};";
                    }
                    else if (tipoImportacao == Enumeradores.TipoImportacao.Credito)
                    {
                        strRowInfo = $"{DbRotinas.ConverterParaString(lastRow["numero do documento"])};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["nr cliente"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["descrição"])};" +
                          $"{DbRotinas.ConverterParaDatetime(lastRow["data do credito"]):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaString(lastRow["valor do credito"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["obs"])};" +
                          $"{DbRotinas.ConverterParaDecimal(lastRow["saldo do credito"]):C};";
                    }
                    else if (tipoImportacao == Enumeradores.TipoImportacao.Boleto)
                    {
                        strRowInfo = $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Companhia"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["NumerodaFatura"])};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Numerodocliente"]))};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["datavencimento"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaDecimal(DbRotinas.ConverterParaString(lastRow["valorfatura"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["linhadigitavel"])};" +
                          $"{DbRotinas.ConverterParaDatetime(DbRotinas.ConverterParaString(lastRow["datadocumento"])):dd/MM/yyyy};" +
                          $"{DbRotinas.ConverterParaString(lastRow["endereço"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numero"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["complemento"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["bairro"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cep"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cidade"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["estado"])};";
                    }
                    else if (tipoImportacao == Enumeradores.TipoImportacao.ClienteEndereco)
                    {
                        strRowInfo = $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["Contrato"]))};" +
                          $"{DbRotinas.Descriptografar(DbRotinas.ConverterParaString(lastRow["an8cobranca"]))};" +
                          $"{DbRotinas.ConverterParaString(lastRow["enderecocob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numerocob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["complementocob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["bairrocob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cepcob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cidadecob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["estadocob"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["an8obra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["enderecoobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["numeroobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["complementoobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["bairroobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cepobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["estadoobra"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["mensagemcontrato"])};" +
                          $"{DbRotinas.ConverterParaString(lastRow["cepobra"])};";
                    }
                }

                var Msg = $"{Environment.NewLine}Ocorreu um erro na leitura do arquivo - {Environment.NewLine}" +
                      $"Ln: {NrLinha} | Informação: {strRowInfo}{Environment.NewLine}" +
                      $"Ln: {NrLinha + 1} | Informação: ** Erro ao ler a informação desta linha **" +
                      $"{Environment.NewLine}";

                return Msg;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }
    }
}
