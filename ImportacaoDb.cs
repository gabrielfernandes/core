using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public static class ImportacaoDb
    {
        public static OperacaoDbRetorno ProcessarCliente(int codImportacao)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            junix_cobrancaContext Context = new junix_cobrancaContext();

            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));

                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    connection.Close();

                }
                return op;
            }
        }
        public static OperacaoDbRetorno ProcessarClienteEndereco(int codImportacao)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            junix_cobrancaContext Context = new junix_cobrancaContext();
            int totalResitrosPendentes = 0;

            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_ENDERECO_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                        codImportacao = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                        op.Dados = codImportacao;
                    }
                }
                catch (Exception ex)
                {
                    var processamento = Context.Importacaos.Where(x => x.Cod == codImportacao).FirstOrDefault();
                    if (processamento != null)
                    {
                        processamento.Status = (short)Enumeradores.StatusImportacao.Erro;
                        processamento.Mensagem = $"Erro no processamento - Mensagem:{ex.Message}";
                        Context.SaveChanges();
                    }
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    connection.Close();

                }
            }
            return op;
        }
        public static void ProcessarCredito(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CREDITO_PROCESSAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }
            }
        }
        public static OperacaoDbRetorno ImportacaoBoletoProcessar(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_BOLETO_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));

                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var processamento = Context.Importacaos.Where(x => x.Cod == codImportacao).FirstOrDefault();
                    if (processamento != null)
                    {
                        processamento.Status = (short)Enumeradores.StatusImportacao.Erro;
                        processamento.Mensagem = $"Erro no processamento - Mensagem:{ex.Message}";
                        Context.SaveChanges();
                    }
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return op;
        }
        public static OperacaoDbRetorno ImportacaoParcelaProcessar(int codImportacao)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_PROCESSAR_PAGINADO_ETAPA_PARCELA", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                var processamento = Context.Importacaos.Where(x => x.Cod == codImportacao).FirstOrDefault();
                                if (processamento != null)
                                {
                                    processamento.Status = (short)Enumeradores.StatusImportacao.Erro;
                                    processamento.Mensagem = $"Erro no processamento - Mensagem:{ex.Message}";
                                    Context.SaveChanges();
                                }
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }
        #region BULKCOPY

        public static void BulkCopy(DataTable dados, string Tabela, List<ColunaDePara> Colunas)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();


            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                SqlTransaction transaction = null;

                connection.Open();
                try
                {
                    transaction = connection.BeginTransaction();

                    using (var sqlBulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock, transaction))
                    {
                        sqlBulkCopy.BulkCopyTimeout = 360;
                        sqlBulkCopy.DestinationTableName = Tabela;

                        foreach (var item in Colunas)
                        {
                            sqlBulkCopy.ColumnMappings.Add(item.NomeColunaDataTable, item.NomeColunaTabela);
                        }

                        sqlBulkCopy.WriteToServer(dados);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    var msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = $"{msg} | {ex.InnerException} {ex.InnerException.Message}";
                        throw new Exception(msg, ex);
                    }
                    ex = new Exception($"Erro ao colar os dados: {ex.Message}", ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }
        }

        //public static void BulkCopy(DataTable dados, string Tabela, List<ColunaDePara> Colunas)
        //{
        //    junix_cobrancaContext Context = new junix_cobrancaContext();

        //    using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
        //    {
        //        SqlTransaction transaction = null;
        //        SqlBulkCopy bulkCopy = null;
        //        DataTable dataTable = new DataTable();
        //        connection.Open();
        //        try
        //        {
        //            transaction = connection.BeginTransaction();

        //            using (var sqlBulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock, transaction))
        //            {
        //                sqlBulkCopy.BulkCopyTimeout = 360;
        //                sqlBulkCopy.DestinationTableName = Tabela;

        //                foreach (var item in Colunas)
        //                {
        //                    sqlBulkCopy.ColumnMappings.Add(item.NomeColunaDataTable, item.NomeColunaTabela);
        //                }

        //                sqlBulkCopy.WriteToServer(dados);
        //            }
        //            transaction.Commit();
        //        }
        //        catch (Exception exception)
        //        {
        //            // loop through all inner exceptions to see if any relate to a constraint failure
        //            transaction.Rollback();
        //            bool dataExceptionFound = false;
        //            Exception tmpException = exception;
        //            while (tmpException != null)
        //            {
        //                if (tmpException is SqlException
        //                   && tmpException.Message.Contains("constraint"))
        //                {
        //                    dataExceptionFound = true;
        //                    break;
        //                }

        //                if (tmpException.InnerException != null)
        //                {
        //                    string errorMessage = $"Message: {tmpException.Message} | InnerException.Message: { tmpException.InnerException.Message}";
        //                    throw new Exception(errorMessage, exception);
        //                }

        //                tmpException = tmpException.InnerException;
        //            }
        //            if (dataExceptionFound)
        //            {
        //                // call the helper method to document the errors and invalid data
        //                string errorMessage = GetBulkCopyFailedData(
        //                   connection.ConnectionString,
        //                   bulkCopy.DestinationTableName,
        //                   dataTable.CreateDataReader());
        //                throw new Exception(errorMessage, exception);
        //            }
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }

        //    }
        //}
        /// <summary>
        /// Build an error message with the failed records and their related exceptions.
        /// </summary>
        /// <param name="connectionString">Connection string to the destination database</param>
        /// <param name="tableName">Table name into which the data will be bulk copied.</param>
        /// <param name="dataReader">DataReader to bulk copy</param>
        /// <returns>Error message with failed constraints and invalid data rows.</returns>
        public static string GetBulkCopyFailedData(
           string connectionString,
           string tableName,
           IDataReader dataReader)
        {
            StringBuilder errorMessage = new StringBuilder("Bulk copy failures:" + Environment.NewLine);
            SqlConnection connection = null;
            SqlTransaction transaction = null;
            SqlBulkCopy bulkCopy = null;
            DataTable tmpDataTable = new DataTable();

            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                transaction = connection.BeginTransaction();
                bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.CheckConstraints, transaction);
                bulkCopy.DestinationTableName = tableName;

                // create a datatable with the layout of the data.
                DataTable dataSchema = dataReader.GetSchemaTable();
                foreach (DataRow row in dataSchema.Rows)
                {
                    tmpDataTable.Columns.Add(new DataColumn(
                       row["ColumnName"].ToString(),
                       (Type)row["DataType"]));
                }

                // create an object array to hold the data being transferred into tmpDataTable 
                //in the loop below.
                object[] values = new object[dataReader.FieldCount];

                // loop through the source data
                while (dataReader.Read())
                {
                    // clear the temp DataTable from which the single-record bulk copy will be done
                    tmpDataTable.Rows.Clear();

                    // get the data for the current source row
                    dataReader.GetValues(values);

                    // load the values into the temp DataTable
                    tmpDataTable.LoadDataRow(values, true);

                    // perform the bulk copy of the one row
                    try
                    {
                        bulkCopy.WriteToServer(tmpDataTable);
                    }
                    catch (Exception ex)
                    {
                        // an exception was raised with the bulk copy of the current row. 
                        // The row that caused the current exception is the only one in the temp 
                        // DataTable, so document it and add it to the error message.
                        DataRow faultyDataRow = tmpDataTable.Rows[0];
                        errorMessage.AppendFormat("Error: {0}{1}", ex.Message, Environment.NewLine);
                        errorMessage.AppendFormat("Row data: {0}", Environment.NewLine);
                        foreach (DataColumn column in tmpDataTable.Columns)
                        {
                            errorMessage.AppendFormat(
                               "\tColumn {0} - [{1}]{2}",
                               column.ColumnName,
                               faultyDataRow[column.ColumnName].ToString(),
                               Environment.NewLine);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                   "Unable to document SqlBulkCopy errors. See inner exceptions for details.",
                   ex);
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return errorMessage.ToString();
        }
        #endregion

        public static List<ImportacaoStatus> ImportacaoContratoObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CONTRATO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoStatus> ImportacaoContratoObterStatusArquivo(int CodUsuario, int? CodImportacao, string NomeArquivo)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CONTRATO_CONSULTAR_ARQUIVO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(NomeArquivo));
                        //sqlCmd.Parameters.AddWithValue("@GUID", DbRotinas.ConverterDbValue(GUID));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoStatus> ImportacaoStatusAdimplenciaObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_STATUS_ADIMPLENCIA_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoStatus> ImportacaoStatusUnidadeObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_UNIDADE_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoStatus> ImportacaoFaseSinteseObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_FASE_SINTESE_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoStatus> ImportacaoSaldoDevedorObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_SALDO_DEVEDOR_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoStatus> ImportacaoTipoBloqueioObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_TIPO_BLOQUEIO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static OperacaoDbRetorno ImportacaoClienteEnderecoObterStatus(int CodUsuario, int? CodImportacao, string nmFile = "")
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_ENDERECO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(nmFile));

                        CodImportacao = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                        op.Dados = CodImportacao;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return op;
        }

        public static OperacaoDbRetorno ImportacaoClienteObterStatus(int CodUsuario, int? CodImportacao, string nmFile = "")
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(nmFile));

                        CodImportacao = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                        op.Dados = CodImportacao;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return op;
        }
        public static List<ImportacaoStatus> ImportacaoCreditoObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CREDITO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                ID = DbRotinas.ConverterParaString(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static OperacaoDbRetorno ImportacaoBoletoObterStatus(int CodUsuario, int? CodImportacao, string nmFile = "")
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_BOLETO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(nmFile));

                        CodImportacao = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                        op.Dados = CodImportacao;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return op;
        }

        public static OperacaoDbRetorno ImportacaoParcelaObterStatus(int CodUsuario, int? CodImportacao, string nmFile = "")
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(nmFile));

                        CodImportacao = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                        op.Dados = CodImportacao;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return op;
        }
        public static List<ImportacaoStatus> ImportacaoPrestacaoServicoObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PRESTACAO_SERVICO_CONSULTAR", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }


        public static List<ImportacaoStatus> ImportacaoClienteObterStatusArquivo(int CodUsuario, int? CodImportacao, string NomeArquivo)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_CONSULTAR_ARQUIVO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        sqlCmd.Parameters.AddWithValue("@NomeArquivo", DbRotinas.ConverterDbValue(NomeArquivo));
                        //sqlCmd.Parameters.AddWithValue("@GUID", DbRotinas.ConverterDbValue(GUID));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoStatus> ImportacaoDonoCarteiraObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("[SP_IMPORTACAO_DONO_CARTEIRA_CONSULTAR]", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoStatus> ImportacaoParticipacaoObterStatus(int CodUsuario, int? CodImportacao)
        {
            //
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoStatus> lista = new List<ImportacaoStatus>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("[SP_IMPORTACAO_PARTICIPACAO_CONSULTAR]", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario));
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(CodImportacao));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoStatus()
                            {
                                CodImportacao = DbRotinas.ConverterParaInt(dr["CodImportacao"]),
                                Status = DbRotinas.ConverterParaShort(dr["Status"]),
                                Total = DbRotinas.ConverterParaShort(dr["Total"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoContratoResultado> ListarResultadoPorContrato(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoContratoResultado> lista = new List<ImportacaoContratoResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_RESULTADO_POR_CONTRATO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoContratoResultado()
                            {
                                Bloco = DbRotinas.ConverterParaString(dr["Bloco"]),
                                CodBloco = DbRotinas.ConverterParaString(dr["CodBloco"]),
                                CodEmpreendimento = DbRotinas.ConverterParaString(dr["CodEmpreendimento"]),
                                CodigoCliente = DbRotinas.ConverterParaString(dr["CodigoCliente"]),
                                CodigoRegional = DbRotinas.ConverterParaString(dr["CodigoRegional"]),
                                DataVenda = DbRotinas.ConverterParaString(dr["DataVenda"]),
                                NomeEmpreendimento = DbRotinas.ConverterParaString(dr["NomeEmpreendimento"]),
                                NumeroContrato = DbRotinas.ConverterParaString(dr["NumeroContrato"]),
                                NumeroUnidade = DbRotinas.ConverterParaString(dr["NumeroUnidade"]),
                                Regional = DbRotinas.ConverterParaString(dr["Regional"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"]),
                                ValorVenda = DbRotinas.ConverterParaString(dr["ValorVenda"]),

                                DataEntregaChaves = DbRotinas.ConverterParaString(dr["DataEntregaChaves"]),
                                DataPrevisaoSGI = DbRotinas.ConverterParaString(dr["DataPrevisaoSGI"]),
                                DataPromessaEntrega = DbRotinas.ConverterParaString(dr["DataPromessaEntrega"]),
                                Juridico = DbRotinas.ConverterParaString(dr["Juridico"]),
                                StatusNegativacao = DbRotinas.ConverterParaString(dr["StatusNegativacao"]),
                                StatusObra = DbRotinas.ConverterParaString(dr["StatusObra"]),
                                StatusRepasseSimplificado = DbRotinas.ConverterParaString(dr["StatusRepasseSimplificado"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoContratoResultado> ListarResultadoPorFaseSintese(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoContratoResultado> lista = new List<ImportacaoContratoResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_FASE_SINTESE_POR_CONTRATO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoContratoResultado()
                            {
                                CodSAP = DbRotinas.ConverterParaString(dr["CodSAP"]),
                                DataFaseSintese = DbRotinas.ConverterParaString(dr["DataStatusFaseSintese"]),
                                Etapa = DbRotinas.ConverterParaString(dr["Etapa"]),
                                Fase = DbRotinas.ConverterParaString(dr["Fase"]),
                                Sintese = DbRotinas.ConverterParaString(dr["Sintese"]),
                                Observacao = DbRotinas.ConverterParaString(dr["Observacao"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoCreditoResultado> ListarResultadoPorCredito(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoCreditoResultado> lista = new List<ImportacaoCreditoResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CREDITO_RESULTADO_POR_CREDITO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoCreditoResultado()
                            {
                                Cod = DbRotinas.ConverterParaString(dr["Cod"]),
                                NumeroDocumento = DbRotinas.ConverterParaString(dr["NumeroDocumento"]),
                                NumeroCliente = DbRotinas.ConverterParaString(dr["NumeroCliente"]),
                                Descricao = DbRotinas.ConverterParaString(dr["Descricao"]),
                                DataCredito = DbRotinas.ConverterParaString(dr["DataCredito"]),
                                ValorCredito = DbRotinas.ConverterParaString(dr["ValorCredito"]),
                                Observacao = DbRotinas.ConverterParaString(dr["Observacao"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"])


                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoBoletoResultado> ListarResultadoPorBoleto(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoBoletoResultado> lista = new List<ImportacaoBoletoResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_BOLETO_RESULTADO_POR_PARCELA", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoBoletoResultado()
                            {
                                Cod = DbRotinas.ConverterParaString(dr["Cod"]),
                                Filial = DbRotinas.ConverterParaString(dr["Filial"]),
                                NumeroFatura = DbRotinas.ConverterParaString(dr["NumeroFatura"]),
                                NumeroCliente = DbRotinas.ConverterParaString(dr["NumeroCliente"]),
                                DataVencimento = DbRotinas.ConverterParaString(dr["DataVencimento"]),
                                ValorFatura = DbRotinas.ConverterParaString(dr["ValorFatura"]),
                                LinhaDigitavel = DbRotinas.ConverterParaString(dr["LinhaDigitavel"]),
                                CodCliente = DbRotinas.ConverterParaString(dr["CodCliente"]),
                                CodParcela = DbRotinas.ConverterParaString(dr["CodParcela"]),
                                Status = DbRotinas.ConverterParaString(dr["Status"]),
                                Mensagem = DbRotinas.ConverterParaString(dr["Mensagem"]),
                                DataProcessamento = DbRotinas.ConverterParaString(dr["DataProcessamento"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"])


                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
        public static List<ImportacaoParcelaResultado> ListarResultadoPorParcela(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoParcelaResultado> lista = new List<ImportacaoParcelaResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_RESULTADO_POR_PARCELA", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoParcelaResultado()
                            {
                                Cod = DbRotinas.ConverterParaString(dr["Cod"]),
                                Companhia = DbRotinas.ConverterParaString(dr["Companhia"]),
                                Filial = DbRotinas.ConverterParaString(dr["Filial"]),
                                FilialDescricao = DbRotinas.ConverterParaString(dr["FilialDescricao"]),
                                RotaArea = DbRotinas.ConverterParaString(dr["RotaArea"]),
                                Status = DbRotinas.ConverterParaString(dr["Status"]),
                                TipoContrato = DbRotinas.ConverterParaString(dr["TipoContrato"]),
                                NumeroCliente = DbRotinas.ConverterParaString(dr["NumeroCliente"]),
                                CPFCNPJ = DbRotinas.ConverterParaString(dr["CPFCNPJ"]),
                                Descricao = DbRotinas.ConverterParaString(dr["Descricao"]),
                                NumeroFatura = DbRotinas.ConverterParaString(dr["NumeroFatura"]),
                                ValorAberto = DbRotinas.ConverterParaString(dr["ValorAberto"]),
                                DataVencimento = DbRotinas.ConverterParaString(dr["DataVencimento"]),
                                NumeroParcela = DbRotinas.ConverterParaString(dr["NumeroParcela"]),
                                Governo = DbRotinas.ConverterParaString(dr["Governo"]),
                                Corporativo = DbRotinas.ConverterParaString(dr["Corporativo"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"]),
                                classificacaofatura = DbRotinas.ConverterParaString(dr["CodSinteseCliente"]),
                                tipodocumento = DbRotinas.ConverterParaString(dr["TipoDocumento"]),
                                instrumentopagto = DbRotinas.ConverterParaString(dr["InstrumentoPgto"]),
                                datapagamento = DbRotinas.ConverterParaString(dr["DataPagamento"]),
                                Mensagem = DbRotinas.ConverterParaString(dr["Mensagem"])


                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoClienteResultado> ListarResultadoPorCliente(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoClienteResultado> lista = new List<ImportacaoClienteResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_RESULTADO_POR_CLIENTE", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoClienteResultado()
                            {
                                Cod = DbRotinas.ConverterParaString(dr["Cod"]),
                                NumeroCliente = DbRotinas.ConverterParaString(dr["NumeroCliente"]),
                                DescricaoCliente = DbRotinas.ConverterParaString(dr["DescricaoCliente"]),
                                CpfCnpj = DbRotinas.ConverterParaString(dr["CpfCnpj"]),
                                Endereco = DbRotinas.ConverterParaString(dr["Endereco"]),
                                Numero = DbRotinas.ConverterParaString(dr["Numero"]),
                                Complemento = DbRotinas.ConverterParaString(dr["Complemento"]),
                                Bairro = DbRotinas.ConverterParaString(dr["Bairro"]),
                                CEP = DbRotinas.ConverterParaString(dr["CEP"]),
                                Cidade = DbRotinas.ConverterParaString(dr["Cidade"]),
                                Estado = DbRotinas.ConverterParaString(dr["Estado"]),
                                TP = DbRotinas.ConverterParaString(dr["TP"]),
                                DDD = DbRotinas.ConverterParaString(dr["DDD"]),
                                NumeroTelefone = DbRotinas.ConverterParaString(dr["NumeroTelefone"]),
                                Email = DbRotinas.ConverterParaString(dr["Email"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }

        public static List<ImportacaoClienteEnderecoResultado> ListarResultadoPorClienteEndereco(int codImportacao, short status)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            List<ImportacaoClienteEnderecoResultado> lista = new List<ImportacaoClienteEnderecoResultado>();
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_RESULTADO_POR_CLIENTE_ENDERECO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.Parameters.AddWithValue("@Status", DbRotinas.ConverterDbValue(status));
                        var dr = sqlCmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista.Add(new ImportacaoClienteEnderecoResultado()
                            {
                                Cod = DbRotinas.ConverterParaString(dr["Cod"]),
                                Contrato = DbRotinas.ConverterParaString(dr["Contrato"]),
                                EnderecoCob = DbRotinas.ConverterParaString(dr["EnderecoCob"]),
                                NumeroCob = DbRotinas.ConverterParaString(dr["NumeroCob"]),
                                ComplementoCob = DbRotinas.ConverterParaString(dr["ComplementoCob"]),
                                BairroCob = DbRotinas.ConverterParaString(dr["BairroCob"]),
                                CEPCob = DbRotinas.ConverterParaString(dr["CEPCob"]),
                                CidadeCob = DbRotinas.ConverterParaString(dr["CidadeCob"]),
                                EstadoCob = DbRotinas.ConverterParaString(dr["EstadoCob"]),
                                EnderecoObra = DbRotinas.ConverterParaString(dr["EnderecoObra"]),
                                NumeroObra = DbRotinas.ConverterParaString(dr["NumeroObra"]),
                                ComplementoObra = DbRotinas.ConverterParaString(dr["ComplementoObra"]),
                                BairroObra = DbRotinas.ConverterParaString(dr["BairroObra"]),
                                CepObra = DbRotinas.ConverterParaString(dr["CepObra"]),
                                CidadeObra = DbRotinas.ConverterParaString(dr["CidadeObra"]),
                                EstadoObra = DbRotinas.ConverterParaString(dr["EstadoObra"]),
                                Mensagem = DbRotinas.ConverterParaString(dr["Mensagem"]),
                                Resultado = DbRotinas.ConverterParaString(dr["Resultado"])
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();

                }

            }
            return lista;
        }
    }
    public class ColunaDePara
    {
        public string NomeColunaTabela { get; set; }
        public string NomeColunaDataTable { get; set; }
    }
    public class ImportacaoStatus
    {
        public string ID { get; set; }
        public int CodImportacao { get; set; }
        public int Total { get; set; }
        public short Status { get; set; }
        public string StatusDesc { get { return EnumeradoresDescricao.StatusImportacaoParcela(Status); } }
    }
    public class ImportacaoParcelaResultado
    {
        public string Cod { get; set; }
        public string CodImportacao { get; set; }
        public string Companhia { get; set; }
        public string Filial { get; set; }
        public string FilialDescricao { get; set; }
        public string RotaArea { get; set; }
        public string Status { get; set; }
        public string TipoContrato { get; set; }
        public string NumeroCliente { get; set; }
        public string CPFCNPJ { get; set; }
        public string Descricao { get; set; }
        public string NumeroFatura { get; set; }
        public string ValorAberto { get; set; }
        public string DataVencimento { get; set; }
        public string DataFechamento { get; set; }
        public string NumeroParcela { get; set; }
        public string Governo { get; set; }
        public string Corporativo { get; set; }
        public string Mensagem { get; set; }
        public string classificacaofatura { get; set; }
        public string notafiscal { get; set; }
        public string tipodocumento { get; set; }
        public string instrumentopagto { get; set; }
        public string datapagamento { get; set; }
        public string Resultado { get; set; }
    }
    public class ImportacaoCreditoResultado
    {
        public string Cod { get; set; }
        public string CodImportacao { get; set; }
        public string NumeroDocumento { get; set; }
        public string NumeroCliente { get; set; }
        public string Descricao { get; set; }
        public string DataCredito { get; set; }
        public string ValorCredito { get; set; }
        public string Observacao { get; set; }
        public string Status { get; set; }
        public string Mensagem { get; set; }

        public string Resultado { get; set; }
    }
    public class ImportacaoClienteResultado
    {
        public string Cod { get; set; }
        public string NumeroCliente { get; set; }
        public string DescricaoCliente { get; set; }
        public string CpfCnpj { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string TP { get; set; }
        public string DDD { get; set; }
        public string NumeroTelefone { get; set; }
        public string Email { get; set; }
        public string Resultado { get; set; }

        public string Mensagem { get; set; }

    }
    public class ImportacaoClienteEnderecoResultado
    {
        public string Cod { get; set; }
        public string Contrato { get; set; }
        public string EnderecoCob { get; set; }
        public string NumeroCob { get; set; }
        public string ComplementoCob { get; set; }
        public string BairroCob { get; set; }
        public string CEPCob { get; set; }
        public string CidadeCob { get; set; }
        public string EstadoCob { get; set; }
        public string EnderecoObra { get; set; }
        public string NumeroObra { get; set; }
        public string ComplementoObra { get; set; }
        public string BairroObra { get; set; }
        public string CepObra { get; set; }
        public string CidadeObra { get; set; }
        public string EstadoObra { get; set; }
        public string Resultado { get; set; }

        public string Mensagem { get; set; }

    }
    public class ImportacaoBoletoResultado
    {
        public string Cod { get; set; }
        public string CodImportacao { get; set; }
        public string Filial { get; set; }
        public string NumeroFatura { get; set; }
        public string NumeroCliente { get; set; }
        public string DataVencimento { get; set; }
        public string ValorFatura { get; set; }
        public string LinhaDigitavel { get; set; }
        public string CodCliente { get; set; }
        public string CodParcela { get; set; }
        public string Status { get; set; }
        public string Mensagem { get; set; }
        public string DataProcessamento { get; set; }
        public string Resultado { get; set; }
    }
    public class ImportacaoContratoResultado
    {
        public string CodSAP { get; set; }
        public string TipoBloqueio { get; set; }
        public string CodigoUnidade { get; set; }
        public string Gestao { get; set; }
        public string CodEmpresa { get; set; }
        public string Empresa { get; set; }
        public string CodigoRegional { get; set; }
        public string Regional { get; set; }
        public string CodEmpreendimento { get; set; }
        public string NomeEmpreendimento { get; set; }
        public string CodBloco { get; set; }
        public string Bloco { get; set; }
        public string NumeroUnidade { get; set; }
        public string NumeroContrato { get; set; }
        public string ValorVenda { get; set; }
        public string DataVenda { get; set; }
        public string CodigoCliente { get; set; }
        public string Resultado { get; set; }

        public string DataPromessaEntrega { get; set; }
        public string Juridico { get; set; }
        public string StatusObra { get; set; }
        public string StatusRepasseSimplificado { get; set; }
        public string DataEntregaChaves { get; set; }
        public string DAF { get; set; }
        public string MetroQuadrado { get; set; }
        public string DataPrevisaoSGI { get; set; }
        public string StatusNegativacao { get; set; }
        public string DataStatusInadimplente { get; set; }
        public string StatusInadimplente { get; set; }
        public string StatusUnidade { get; set; }
        public string DataSaldoDevedor { get; set; }
        public string ValorSaldoDevedor { get; set; }
        public string DataFaseSintese { get; set; }
        public string Etapa { get; set; }
        public string Fase { get; set; }
        public string Sintese { get; set; }
        public string Observacao { get; set; }

    }
}
