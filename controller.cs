[HttpPost]
public JsonResult ExportarExcel(DTParameters param)
{
    OperacaoRetorno op = new OperacaoRetorno();
    var filter = WebRotinas.FiltroGeralObter();
    try
    {
        string arquivo = WebRotinas.ObterArquivoTemp($"boletos{DateTime.Now:ddMMyyyyss}.csv");

        var dtsource = ListaDadosSolicitacao(filter, CodFase, CodSintese, CodSinteseHome);
        var result = WebRotinas.DTRotinas.DTFilterResult(dtsource, param);

        List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
        columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Regional", Text = "Filial" });
        columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodCliente", Text = "AN8" });
    
        ConverterListaParaExcel.ConvertListToExcelFile(result.ToList(), arquivo, columns);

        ConverterListaParaExcel.GerarCSVDeEspecList(result.ToList(), arquivo, columns);
        FileInfo f = new FileInfo(arquivo);

        op.OK = true;
        op.Dados = Url.Content($"~/temp/{f.Name}");
    }
    catch (Exception ex)
    {
        op = new OperacaoRetorno(ex);
    }
    return Json(op);
}