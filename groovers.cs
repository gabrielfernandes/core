 public static DataTable ToDataTable<T>(List<T> items)
{
    DataTable dataTable = new DataTable(typeof(T).Name);
    //Get all the properties
    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
    foreach (PropertyInfo prop in Props)
    {
        //Setting column names as Property names
        dataTable.Columns.Add(prop.Name);
    }
    foreach (T item in items)
    {
        var values = new object[Props.Length];
        for (int i = 0; i < Props.Length; i++)
        {
            //inserting property values to datatable rows
            values[i] = Props[i].GetValue(item, null);
        }
        dataTable.Rows.Add(values);
    }
    //put a breakpoint here and check datatable
    return dataTable;
}
public static void ToDataTable2<T>(List<T> list, string csvNameWithExt, List<CollumnExcel> columns)
{
    DataTable dt = new DataTable(typeof(T).Name);
    if (list == null || list.Count == 0) return;
    //Get all the properties
    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
    PropertyInfo[] props = GetProperties(list[0]);
    CollumnExcel columnTitle = new CollumnExcel();
    foreach (var column in columns)
    {
        if (props.Select(x => x.Name).Contains(column.Key))
            dt.Columns.Add(column.Text);
    }
    foreach (T item in list)
    {
        DataRow row = dt.NewRow();
        foreach (CollumnExcel column in columns)
        {
            if (props.Select(x => x.Name).Contains(column.Key))
            {
                row[column.Text] = Convert.ToString(item.GetType().GetProperty(column.Key).GetValue(item, null)).Replace(';', ' ');
            }
        }
        dt.Rows.Add(row);
    }
    DataSet ds = new DataSet();
    ds.Tables.Add(dt);
    bool result = CreateExcelDocument(ds, csvNameWithExt);
    ds.Tables.Remove(dt);
}
public static string ObterArquivoTemp(string fileTemp = null)
{
    string fullPath = HttpContext.Current.Server.MapPath("~/temp");

    WebRotinas.CriarDiretorioSemErro(fullPath);

    if (string.IsNullOrEmpty(fileTemp)) { fileTemp = Path.GetTempFileName(); };

    return Path.Combine(fullPath, Path.GetFileName(fileTemp));
}

/// <summary>
/// Metodo para Gerar um CSV a partir de qulaque list
/// </summary>
/// <param name="list">Lista de qualquer informação.</param>
/// <param name="csvNameWithExt">Local do Arquivo.</param>
public static void GerarCSVDeGenericList<T>(List<T> list, string csvNameWithExt)
{
    if (list == null || list.Count == 0) return;
    string newLine = Environment.NewLine;

    using (var sw = new StreamWriter(csvNameWithExt, false, Encoding.UTF8))
    {
        PropertyInfo[] props = GetProperties(list[0]);
        //PropertyInfo[] propsFiltradas = props.Where(x => colunas.Contains(x.Name)).ToArray();

        foreach (PropertyInfo pi in props)
        {
            sw.Write(Idioma.Traduzir(pi.Name) + ";");
        }
        sw.Write(newLine);

        foreach (T item in list)
        {
            foreach (PropertyInfo pi in props)
            {
                string whatToWrite =
                Convert.ToString(item.GetType()
                                     .GetProperty(pi.Name) 
                                     .GetValue(item, null))
                    .Replace(';', ' ') + ';';

                sw.Write(whatToWrite);
            }
            sw.Write(newLine);
        }
    }
}

/// <summary>
/// Metodo para Gerar um CSV a partir de qulaque list
/// </summary>
/// <param name="list">Lista de qualquer informação.</param>
/// <param name="csvNameWithExt">Local do Arquivo.</param>
/// <param name="columns">A partir das colunas enviadas.</param>
public static void GerarCSVDeEspecList<T>(List<T> list, string csvNameWithExt, List<CollumnExcel> columns)
{
    if (list == null || list.Count == 0) return;
    string newLine = Environment.NewLine;

    using (var sw = new StreamWriter(csvNameWithExt, false, Encoding.UTF8))
    {
        PropertyInfo[] props = GetProperties(list[0]);
        CollumnExcel columnTitle = new CollumnExcel();
        foreach (var column in columns)
        {
            if (props.Select(x => x.Name).Contains(column.Key))
            {
                columnTitle = column;
                sw.Write(Idioma.Traduzir(columnTitle.Text) + ";");
            }

        }
        sw.Write(newLine);

        foreach (T item in list)
        {
            foreach (CollumnExcel column in columns)
            {
                if (props.Select(x => x.Name).Contains(column.Key))
                {
                    string whatToWrite =
                    Convert.ToString(item.GetType()
                                         .GetProperty(column.Key)
                                         .GetValue(item, null))
                        .Replace(';', ' ') + ';';
                    sw.Write(whatToWrite);
                }

            }
            sw.Write(newLine);
        }
    }
}

public static PropertyInfo[] GetProperties(object obj)
{
    return obj.GetType().GetProperties();
}

public static List<QueryBuilderRuleModel> ConvertClassToQueryBuilderRules<T>(T builder)
{
    List<QueryBuilderRuleModel> rules = new List<QueryBuilderRuleModel>();
    foreach (PropertyInfo propertyInfo in builder.GetType().GetProperties())
    {

        object[] attrs = propertyInfo.GetCustomAttributes(true);
        var qryBuilder = attrs.OfType<AttributesValues.IsQueryBuilder>().FirstOrDefault();
        if (qryBuilder != null)
        {
            if (qryBuilder.Value)
            {
                if (attrs.FirstOrDefault() != null)
                {
                    var value = attrs.OfType<DisplayNameAttribute>().FirstOrDefault();
                    var inputType = attrs.OfType<AttributesValues.RulesInput>().FirstOrDefault();
                    var inputValues = attrs.OfType<AttributesValues.RulesValue>().FirstOrDefault();
                    var operators = attrs.OfType<AttributesValues.RulesOperator>().FirstOrDefault();
                    rules.Add(new QueryBuilderRuleModel(
                        propertyInfo.Name,
                        value.DisplayName,
                        CastPropertyTypeToQueryBuilderType(propertyInfo.PropertyType),
                        inputValues != null ? inputValues.Values.ToArray() : null,
                        inputType != null ? inputType.Name : null,
                        operators != null ? operators.Operators.Select(x => CastOperatorsToQueryBuilderOperators(x)).ToArray() : null
                    ));
                }
            }
        }
    }
    return rules;
}

public static void CriarDiretorioSemErro(string fullPath)
{
    try
    {
        if (!Directory.Exists(fullPath))
            Directory.CreateDirectory(fullPath);
    }
    catch (Exception ex)
    {
    }
}

private OperacaoDbRetorno ImportacaoClienteEndereco()
{
    LogServico _log = new LogServico();
    string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoPendente"];
    string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoProcessado"];
    string CaminhoSwapClienteEndereco = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoSwap"];
    try
    {
        DirectoryInfo pendente = new DirectoryInfo(CreateClienteEnderecoCommand._caminhoPendente);
        FileInfo[] Files = pendente.GetFiles();
        OperacaoDbRetorno retorno = new OperacaoDbRetorno();

        foreach (var arquivo in Files)
        {
            if (!arquivo.IsReadOnly)
            {
                //VERIFICAR SE O ARQUIVO ESTA CRESCENDO
                if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.ClienteEndereco)) continue;

                //LOG
                _logHandler.Handle(new RegisterLogCommand($"Arquivo ({arquivo.Name}) encontrado que não esteja no modo somente leitura!"));

                //MOVENDO PARA PASTA SWAP
                importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapClienteEndereco, "");
                DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapClienteEndereco);

                //LOG
                _logHandler.Handle(new RegisterLogCommand($"Arquivo foi movido para o diretório ({Swap.Name})"));

                FileInfo[] Arquivos = Swap.GetFiles();
                foreach (var processar in Arquivos)
                {
                    try
                    {
                        //LOG
                        _logHandler.Handle(new RegisterLogCommand($"Inicio da leitura do arquivo ({arquivo.Name})"));

                        List<CreateClienteEnderecoCommand> Commands = new List<CreateClienteEnderecoCommand>();

                        //TRANSFORMAR ARQUIVO EM CLASSE
                        using (StreamReader sr = new StreamReader(processar.FullName, Encoding.Default, false, 512))
                        {
                            string linhaHader = sr.ReadLine();// NAO NECESSITA DO HEADER

                            var lines = sr.ReadToEnd().Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Skip(1);

                            foreach (var item in lines)
                            {
                                try
                                {
                                    var values = item.Split(';');
                                    var data = new CreateClienteEnderecoCommand
                                    {
                                        NumeroContrato = DbRotinas.Descriptografar(values[0].Trim()),
                                        An8Cob = values[1].Trim(),
                                        EnderecoCob = values[2].Trim(),
                                        NumeroCob = values[3].Trim(),
                                        ComplementoCob = values[4].Trim(),
                                        BairroCob = values[5].Trim(),
                                        CepCob = values[6].Trim(),
                                        CidadeCob = values[7].Trim(),
                                        EstadoCob = values[8].Trim(),
                                        An8Obra = values[9].Trim(),
                                        EnderecoObra = values[10].Trim(),
                                        NumeroObra = values[11].Trim(),
                                        ComplementoObra = values[12].Trim(),
                                        BairroObra = values[13].Trim(),
                                        CepObra = values[14].Trim(),
                                        CidadeObra = values[15].Trim(),
                                        EstadoObra = values[16].Trim(),
                                        MensagemContrato = values[17].Trim(),
                                        ConteudoOriginal = item
                                    };
                                    Commands.Add(data);
                                }
                                catch (Exception ex)
                                {
                                    Commands.Add(new CreateClienteEnderecoCommand() { ImportStatus = "ERRO", ConteudoOriginal = item, ImportMensagem = $"{ex.Message} - {ex.InnerException}" });
                                }
                            }
                        }

                        _logHandler.Handle(new RegisterLogCommand($"Leitura Realizada, processando arquivo ({arquivo.Name})"));

                        //Processar o arquivo
                        var resultado = _clienteEnderecoHandlers.Handle(new ListCreateClienteEnderecoCommand(Commands));

                        //Criar Arquivo de Resultado
                        _importResultHandlers.Handle(new CreateResultFileCommand(processar.FullName, $"{processar.Name}Result.txt", (List<string>)resultado.Data));
                        
                        int CodRetorno = 1;
                        
                        //mover arquivo original
                        _importResultHandlers.Handle(new MoveResultCommand(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno.ToString()));

                        //mover arquivo resultado
                        _importResultHandlers.Handle(new MoveResultCommand($"{processar.Name}Result.txt", processar.FullName, CaminhoProcessado, CodRetorno.ToString()));

                        //REMOVER arquivo do monitoramento
                        _tbMonitorArquivo.Remove(arquivo.Name);

                    }
                    catch (Exception)
                    {
                        #region Mover arquivo para Pasta com Erro
                        string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                        if (!Directory.Exists(CaminhoServer))
                            Directory.CreateDirectory(CaminhoServer);
                        string DestinoServidor = Path.Combine(CaminhoServer, $"Endereco{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                        if (!Directory.Exists(DestinoServidor))
                            Directory.CreateDirectory(DestinoServidor);

                        DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                        FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                        _tbMonitorArquivo.Remove(arquivo.Name);
                        foreach (var item in Aqs)
                        {
                            importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                        }
                        #endregion
                        throw;
                    }
                }

            }
        }
        return retorno;
    }
    catch (Exception ex)
    {
        throw new Exception("Importacao Endereço - ", ex);
    }

}
